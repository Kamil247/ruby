Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: 'users/registrations' }
  root 'posts#index'

  resources :posts
  resources :users
  get 'about' => 'pages#about'
  get 'contact' => 'pages#contact'
  get 'projects' => 'pages#projects'
  get 'account' => 'pages#account'
  get 'admin_panel' => 'pages#admin_panel'


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
