class UsersController < ApplicationController
  respond_to :html, :js

    def index
        @users = User.all
    end
    def destroy
        @user = User.find(params[:id])
        @user.destroy
    
        if @user.destroy
            redirect_to root_url, danger: "User deleted."
        end

    end
     def edit
         @user = User.find(params[:id])
         render('users/registrations/edit_admin')
     end
     def update
            @user = User.find(params[:id])
          if @user.update_attributes(user_params)
            redirect_to admin_panel_url
          end
     end
  def user_params
    params.require(:user).permit(:name, :lastname, :login, :admin)
  end
    def configure_sign_up_params
        devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
      end
    
      # If you have extra params to permit, append them to the sanitizer.
       def configure_account_update_params
         devise_parameter_sanitizer.permit(:create, :update, keys: [:name, :lastname, :login, :admin, :avatar])
         devise_parameter_sanitizer.permit(:update, keys: [:name, :lastname, :login, :admin, :avatar])
        end


    private

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.fetch(:user, {}) .permit(:name, :login, :lastname, :email, :admin, :avatar)
    end
end
