class Post < ApplicationRecord
    has_one_attached :image, dependent: :destroy 
    has_one_attached :photos, dependent: :destroy 
    belongs_to :user
end
